#include <stream9/node_array/node_array.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/node.hpp>
#include <stream9/string.hpp>

namespace testing {

namespace json { using namespace stream9::json; }

using stream9::node_array;
using stream9::node;
using stream9::string;

BOOST_AUTO_TEST_SUITE(node_array_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(default_)
        {
            node_array<int> a;
        }

        BOOST_AUTO_TEST_CASE(with_iterator_)
        {
            std::vector<int> a1 { 1, 2, 3, 4, 5 };

            node_array<int> a2(a1.begin(), a1.end());
        }

        BOOST_AUTO_TEST_CASE(with_initializer_list_)
        {
            node_array<int> a1 { 1, 2, 3, 4, 5 };
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_CASE(assignment_with_ilist_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        a1 = { 4, 5, 6, };

        auto v = json::value_from(a1);
        json::array expected { 4, 5, 6 };
        BOOST_TEST(v == expected);
    }

    BOOST_AUTO_TEST_SUITE(iterator_)

        BOOST_AUTO_TEST_CASE(begin_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            static_assert(std::same_as<decltype(a1.begin()), T::iterator>);

            auto it = a1.begin();

            BOOST_TEST(*it == 1);
        }

        BOOST_AUTO_TEST_CASE(end_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            static_assert(std::same_as<decltype(a1.end()), T::iterator>);

            auto i1 = a1.begin();
            auto i2 = a1.end();

            BOOST_TEST(std::distance(i1, i2) == 3);
        }

        BOOST_AUTO_TEST_CASE(begin_const_)
        {
            using T = node_array<int>;

            T const a1 { 1, 2, 3 };

            static_assert(std::same_as<decltype(a1.begin()), T::const_iterator>);

            auto it = a1.begin();

            BOOST_TEST(*it == 1);
        }

        BOOST_AUTO_TEST_CASE(end_const_)
        {
            using T = node_array<int>;

            T const a1 { 1, 2, 3 };

            auto i1 = a1.begin();
            auto i2 = a1.end();

            static_assert(std::same_as<decltype(a1.end()), T::const_iterator>);

            BOOST_TEST(std::distance(i1, i2) == 3);
        }

    BOOST_AUTO_TEST_SUITE_END() // iterator_

    BOOST_AUTO_TEST_CASE(front_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.front()), int&>);

        BOOST_TEST(a1.front() == 1);
    }

    BOOST_AUTO_TEST_CASE(back_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.back()), int&>);

        BOOST_TEST(a1.back() == 3);
    }

    BOOST_AUTO_TEST_CASE(front_const_)
    {
        using T = node_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.front()), int const&>);

        BOOST_TEST(a1.front() == 1);
    }

    BOOST_AUTO_TEST_CASE(back_const_)
    {
        using T = node_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.back()), int const&>);

        BOOST_TEST(a1.back() == 3);
    }

    BOOST_AUTO_TEST_CASE(at_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.at(0)), int&>);

        BOOST_TEST(a1.at(1) == 2);
    }

    BOOST_AUTO_TEST_CASE(at_error_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        BOOST_CHECK_EXCEPTION(
            a1.at(3),
            stream9::error,
            [&](auto&& e) {
                BOOST_TEST(find_int64(e.context(), "pos") == 3);
                return true;
            });
    }

    BOOST_AUTO_TEST_CASE(at_const_)
    {
        using T = node_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1.at(0)), int const&>);

        BOOST_TEST(a1.at(1) == 2);
    }

    BOOST_AUTO_TEST_CASE(subscript_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1[0]), int&>);

        BOOST_TEST(a1[1] == 2);
    }

    BOOST_AUTO_TEST_CASE(subscript_const_)
    {
        using T = node_array<int>;

        T const a1 { 1, 2, 3 };

        static_assert(std::same_as<decltype(a1[0]), int const&>);

        BOOST_TEST(a1[1] == 2);
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        BOOST_TEST(a1.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        using T = node_array<int>;

        T a1;

        BOOST_TEST(a1.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        BOOST_TEST(!a1.empty());
    }

    BOOST_AUTO_TEST_SUITE(insert_)

        BOOST_AUTO_TEST_CASE(copy_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };
            int i1 = 5;

            auto it1 = a1.insert(a1.begin(), i1);

            BOOST_TEST(a1.size() == 4);
            BOOST_TEST(a1.front() == 5);
            BOOST_CHECK(it1 == a1.begin());
        }

        BOOST_AUTO_TEST_CASE(move_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            auto it1 = a1.insert(a1.begin(), 5);

            BOOST_TEST(a1.size() == 4);
            BOOST_TEST(a1.front() == 5);
            BOOST_CHECK(it1 == a1.begin());
        }

        BOOST_AUTO_TEST_CASE(iterator_range_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };
            std::vector<int> v1 { 4, 5, 6 };

            auto it1 = a1.insert(a1.end(), v1.begin(), v1.end());

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 4, 5, 6 };
            BOOST_TEST(v == expected);
            BOOST_CHECK(it1 == a1.begin() + 3);
        }

#if 0 //TODO range insert
        BOOST_AUTO_TEST_CASE(initializer_list_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            auto it1 = a1.insert(a1.end(), { 4, 5, 6});

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 4, 5, 6 };
            BOOST_TEST(v == expected);
            BOOST_CHECK(it1 == a1.begin() + 3);
        }
#endif

    BOOST_AUTO_TEST_SUITE_END() // insert_

    BOOST_AUTO_TEST_CASE(emplace_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        auto [it1, _] = a1.emplace(a1.end(), 4);

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(it1 == a1.begin() + 3);
    }

    BOOST_AUTO_TEST_CASE(natural_insert_copy_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };
        int i1 = 4;

        auto i = a1.insert(i1);

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(*i == 4);
    }

    BOOST_AUTO_TEST_CASE(natural_insert_move_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        auto i = a1.insert(4); //TODO this seems wrong.

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(*i == 4);
    }

    BOOST_AUTO_TEST_CASE(natural_insert_node_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        auto i = a1.insert(node<int>(4));

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(*i == 4);
    }

    BOOST_AUTO_TEST_CASE(natural_emplace_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };

        auto [_, r] = a1.emplace(4);

        auto v = json::value_from(a1);
        json::array expected { 1, 2, 3, 4, };
        BOOST_TEST(v == expected);
        BOOST_CHECK(r == 4);
    }

    BOOST_AUTO_TEST_CASE(polymorphic_natural_emplace_)
    {
        struct foo
        {
            int v;

            foo(int v) : v { v } {}
            virtual ~foo() = default;
        };

        struct bar : foo
        {
            int w;
            bar(int v) : foo { v } {}
        };

        using T = node_array<foo>;

        T a1;

        auto [_1, r1] = a1.emplace(1);
        static_assert(std::same_as<decltype(r1), foo&>);
        BOOST_TEST(r1.v == 1);

        auto [_2, r2] = a1.emplace<bar>(2);
        static_assert(std::same_as<decltype(r2), bar&>);
        BOOST_TEST(r2.v == 2);
    }

    BOOST_AUTO_TEST_SUITE(erase_)

        BOOST_AUTO_TEST_CASE(single_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            a1.erase(a1.begin() + 1);

            auto v = json::value_from(a1);
            json::array expected { 1, 3 };
            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(range_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            a1.erase(a1.begin(), a1.end());

            auto v = json::value_from(a1);
            json::array expected { };
            BOOST_TEST(v == expected);
        }

        class Object
        {
        public:
            Object(int i) noexcept : m_v { i } {}

            ~Object() noexcept = default;

            Object(Object const&) = delete;
            Object& operator=(Object const&) = delete;
            Object(Object&&) = delete;
            Object& operator=(Object&&) = delete;

            operator int () const noexcept { return m_v; }

        private:
            int m_v;
        };

        void tag_invoke(json::value_from_tag, json::value& v, Object const& o)
        {
            v = static_cast<int>(o);
        }

        BOOST_AUTO_TEST_CASE(predicate_)
        {
            using T = node_array<Object>;

            T a1;
            a1.emplace(1);
            a1.emplace(2);
            a1.emplace(3);
            a1.emplace(4);

            a1.erase_if([](auto&& i) { return i % 2 == 0; });

            auto v = json::value_from(a1);
            json::array expected { 1, 3 };
            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(predicate_with_proj_)
        {
            using T = node_array<Object>;

            T a1;
            a1.emplace(1);
            a1.emplace(2);
            a1.emplace(3);
            a1.emplace(4);

            a1.erase_if(
                [](auto&& i) { return i % 2 == 0; },
                [](auto&& i) { return i + 1; }
            );

            auto v = json::value_from(a1);
            json::array expected { 2, 4 };
            BOOST_TEST(v == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // erase_

    BOOST_AUTO_TEST_SUITE(resize_)

        BOOST_AUTO_TEST_CASE(type_1_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            a1.resize(5);

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 0, 0, };
            BOOST_TEST(v == expected);
        }

        BOOST_AUTO_TEST_CASE(type_2_)
        {
            using T = node_array<int>;

            T a1 { 1, 2, 3 };

            a1.resize(5, 9);

            auto v = json::value_from(a1);
            json::array expected { 1, 2, 3, 9, 9, };
            BOOST_TEST(v == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // resize_

    BOOST_AUTO_TEST_CASE(equal_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };
        T a2 { 1, 2, 3 };
        T a3 { 1 };

        BOOST_CHECK(a1 == a2);
        BOOST_CHECK(a1 != a3);
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        using T = node_array<int>;

        T a1 { 1, 2, 3 };
        T a2 { 1, 2, 4 };

        BOOST_CHECK(a1 < a2);
        BOOST_CHECK(a2 > a1);
    }

    BOOST_AUTO_TEST_CASE(deduction_guide_)
    {
        std::vector<int> v1 { 1, 2, 3 };

        node_array a1(v1.begin(), v1.end());

        auto v2 = json::value_from(a1);
        json::array expected { 1, 2, 3 };
        BOOST_TEST(v2 == expected);
    }

    BOOST_AUTO_TEST_CASE(iter_swap_)
    {
        node_array<string> a;

        a.insert("foo");

        node<string> s { "bar" };

        stream9::iter_swap(a.begin(), s);

        BOOST_TEST(a[0] == "bar");
        BOOST_TEST(s == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // node_array_

} // namespace testing
